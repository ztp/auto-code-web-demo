package com.zengtengpeng.oneToOne.dao;

import com.zengtengpeng.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;


/**
 *一对一用户 dao
 */
@Mapper
public interface TestOneToOneUserDao  extends BaseDao<TestOneToOneUser>{





}
