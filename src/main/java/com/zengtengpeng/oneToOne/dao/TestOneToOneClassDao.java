package com.zengtengpeng.oneToOne.dao;

import com.zengtengpeng.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;


/**
 *一对一班级 dao
 */
@Mapper
public interface TestOneToOneClassDao  extends BaseDao<TestOneToOneClass>{




	/**
	 * 根据一对一用户删除一对一班级
	 */
	public Integer deleteTestOneToOneClassByTestOneToOneUser(TestOneToOneClass testOneToOneClass);

}
