package com.zengtengpeng.oneToOne.service;

import java.util.List;
import com.zengtengpeng.common.service.BaseService;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;
import com.zengtengpeng.oneToOne.dao.TestOneToOneUserDao;


/**
 *一对一用户 service
 */
public interface TestOneToOneUserService extends BaseService<TestOneToOneUser,TestOneToOneUserDao>{




	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	public TestOneToOneUser selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser);
	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	public List<TestOneToOneUser> selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneUser testOneToOneUser);
	/**
	 * 级联删除(根据主键删除) 一对一用户--一对一班级
	 */
	public Integer deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser);

}
