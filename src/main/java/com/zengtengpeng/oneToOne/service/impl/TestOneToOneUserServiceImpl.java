package com.zengtengpeng.oneToOne.service.impl;

import com.zengtengpeng.oneToOne.dao.TestOneToOneClassDao;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zengtengpeng.oneToOne.dao.TestOneToOneUserDao;
import com.zengtengpeng.oneToOne.service.TestOneToOneUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;


/**
 *一对一用户 serverImpl
 */
@Service
@Transactional
public class TestOneToOneUserServiceImpl   implements TestOneToOneUserService {

	/**
	 * 一对一班级
	 */
	@Resource
	private TestOneToOneClassDao testOneToOneClassDao;



	/**
	 * 注入dao
	 */
	@Resource
	private TestOneToOneUserDao testOneToOneUserDao;
	/**
	 * 初始化
	 */
	@Override
	public TestOneToOneUserDao initDao(){
		return testOneToOneUserDao;
	}


	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	@Override
	public TestOneToOneUser selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser){
		testOneToOneUser = this.selectAllByPaging(testOneToOneUser);
		if(testOneToOneUser!=null && testOneToOneUser.getRows()!=null){
			testOneToOneUser.getRows().forEach(t->{
				TestOneToOneUser data= (TestOneToOneUser) t;
				TestOneToOneClass testOneToOneClass=new TestOneToOneClass();
				testOneToOneClass.setUserId(data.getId());
				List<TestOneToOneClass> lists = testOneToOneClassDao.selectByCondition(testOneToOneClass);
				if(lists!=null && lists.size()>0){
					data.setTestOneToOneClass(lists.get(0));
				}
			});
		}
		return testOneToOneUser;

	}


	/**
	 * 级联条件查询 一对一用户--一对一班级
	 */
	@Override
	public List<TestOneToOneUser> selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneUser testOneToOneUser){
		List<TestOneToOneUser> datas = this.selectByCondition(testOneToOneUser);
		if(datas!=null){
			datas.forEach(t->{
				TestOneToOneClass testOneToOneClass=new TestOneToOneClass();
				testOneToOneClass.setUserId(t.getId());
				List<TestOneToOneClass> lists = testOneToOneClassDao.selectByCondition(testOneToOneClass);
				if(lists!=null && lists.size()>0){
					t.setTestOneToOneClass(lists.get(0));
				}
			});
		}
		return datas;

	}


	/**
	 * 级联删除(根据主表删除) 一对一用户--一对一班级
	 */
	@Override
	public Integer deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser){
		TestOneToOneClass testOneToOneClass=new TestOneToOneClass();
		testOneToOneClass.setUserId(testOneToOneUser.getId());
		testOneToOneClassDao.deleteTestOneToOneClassByTestOneToOneUser(testOneToOneClass);
		return testOneToOneUserDao.deleteByPrimaryKey(testOneToOneUser);

	}



}
