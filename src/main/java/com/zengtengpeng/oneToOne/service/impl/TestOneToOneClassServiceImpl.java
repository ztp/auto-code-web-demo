package com.zengtengpeng.oneToOne.service.impl;

import com.zengtengpeng.oneToOne.dao.TestOneToOneUserDao;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zengtengpeng.oneToOne.dao.TestOneToOneClassDao;
import com.zengtengpeng.oneToOne.service.TestOneToOneClassService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;


/**
 *一对一班级 serverImpl
 */
@Service
@Transactional
public class TestOneToOneClassServiceImpl   implements TestOneToOneClassService {

	/**
	 * 一对一班级
	 */
	@Resource
	private TestOneToOneUserDao testOneToOneUserDao;



	/**
	 * 注入dao
	 */
	@Resource
	private TestOneToOneClassDao testOneToOneClassDao;
	/**
	 * 初始化
	 */
	@Override
	public TestOneToOneClassDao initDao(){
		return testOneToOneClassDao;
	}


	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	@Override
	public TestOneToOneClass selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass){
		testOneToOneClass = this.selectAllByPaging(testOneToOneClass);
		if(testOneToOneClass!=null && testOneToOneClass.getRows()!=null){
			testOneToOneClass.getRows().forEach(t->{
				TestOneToOneClass data= (TestOneToOneClass) t;
				TestOneToOneUser testOneToOneUser=new TestOneToOneUser();
				testOneToOneUser.setId(data.getUserId());
				data.setTestOneToOneUser(testOneToOneUserDao.selectByPrimaryKey(testOneToOneUser));
			});
		}
		return testOneToOneClass;

	}


	/**
	 * 级联条件查询一对一用户--一对一班级
	 */
	@Override
	public List<TestOneToOneClass> selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneClass testOneToOneClass){
		List<TestOneToOneClass> datas = this.selectByCondition(testOneToOneClass);
		if(datas!=null){
			datas.forEach(t->{
				TestOneToOneUser testOneToOneUser=new TestOneToOneUser();
				testOneToOneUser.setId(t.getUserId());
				t.setTestOneToOneUser(testOneToOneUserDao.selectByPrimaryKey(testOneToOneUser));
			});
		}
		return datas;

	}


	/**
	 * 级联删除(根据主表删除) 一对一用户--一对一班级
	 */
	@Override
	public Integer deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass){
		testOneToOneClass = testOneToOneClassDao.selectByPrimaryKey(testOneToOneClass);
		if(testOneToOneClass!=null){
			TestOneToOneUser testOneToOneUser=new TestOneToOneUser();
			testOneToOneUser.setId(testOneToOneClass.getUserId());
			testOneToOneUserDao.deleteByPrimaryKey(testOneToOneUser);
		}
		return testOneToOneClassDao.deleteByPrimaryKey(testOneToOneClass);

	}



}
