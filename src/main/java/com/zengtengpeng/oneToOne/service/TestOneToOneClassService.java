package com.zengtengpeng.oneToOne.service;

import java.util.List;
import com.zengtengpeng.common.service.BaseService;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;
import com.zengtengpeng.oneToOne.dao.TestOneToOneClassDao;


/**
 *一对一班级 service
 */
public interface TestOneToOneClassService extends BaseService<TestOneToOneClass,TestOneToOneClassDao>{




	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	public TestOneToOneClass selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass);
	/**
	 * 级联条件查询 一对一用户--一对一班级
	 */
	public List<TestOneToOneClass> selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneClass testOneToOneClass);
	/**
	 * 级联删除(根据主键删除) 一对一用户--一对一班级
	 */
	public Integer deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass);

}
