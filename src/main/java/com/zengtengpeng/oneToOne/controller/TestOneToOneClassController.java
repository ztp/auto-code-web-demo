package com.zengtengpeng.oneToOne.controller;

import com.zengtengpeng.oneToOne.service.TestOneToOneUserService;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;
import java.util.ArrayList;
import javax.annotation.Resource;
import com.zengtengpeng.common.utils.ExcelUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zengtengpeng.common.bean.DataRes;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;
import com.zengtengpeng.oneToOne.service.TestOneToOneClassService;


/**
 *一对一班级 controller
 */
@Api(description="一对一班级")
@Controller
public class TestOneToOneClassController  {

	/**
	 * 一对一用户
	 */
	@Resource
	private TestOneToOneUserService testOneToOneUserService;



	@Resource
	private TestOneToOneClassService testOneToOneClassService;
	/**
	 * 删除-一对一班级
	 */
	@ResponseBody
	@RequestMapping("testOneToOneClass/deleteByPrimaryKey")
	@ApiOperation(value="根据主键删除", notes="参数只用到了主键id,其他参数忽略" ,httpMethod="POST")
	public DataRes deleteByPrimaryKey(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.deleteByPrimaryKey(testOneToOneClass));
	}


	/**
	 *  保存 (主键为空则增加否则修改)-> 一对一班级
	 */
	@ResponseBody
	@RequestMapping("testOneToOneClass/save")
	@ApiOperation(value="保存", notes="主键为空则增加否则修改" ,httpMethod="POST")
	public DataRes save(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		if(testOneToOneClass.getId()==null){
			return DataRes.success(testOneToOneClassService.insert(testOneToOneClass));
		}
		return DataRes.success(testOneToOneClassService.update(testOneToOneClass));

	}


	/**
	 * 根据主键查询->一对一班级
	 */
	@ResponseBody
	@RequestMapping("testOneToOneClass/selectByPrimaryKey")
	@ApiOperation(value="根据主键查询", notes="参数只用到了主键." ,httpMethod="POST")
	public DataRes selectByPrimaryKey(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.selectByPrimaryKey(testOneToOneClass));
	}


	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->一对一班级
	 */
	@ResponseBody
	@RequestMapping("testOneToOneClass/selectByCondition")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByCondition(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.selectByCondition(testOneToOneClass));
	}


	/**
	 * 分页查询 (所有的实体属性都是条件,如果为空则忽略该字段) (详见Page类.所以的实体都继承该类 默认 page=1 pageSize=10)->一对一班级
	 */
	@ResponseBody
	@RequestMapping("testOneToOneClass/selectAllByPaging")
	@ApiOperation(value="分页查询", notes="默认page=1pageSize等于10详见Page类(所有bean都继承该类).参数为空则忽略" ,httpMethod="POST")
	public DataRes selectAllByPaging(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.selectAllByPaging(testOneToOneClass));
	}


	/**
	 * 导出报表->一对一班级
	 */
	@RequestMapping("testOneToOneClass/export")
	@ApiOperation(value="导出excel", notes="导出全部数据.参数为空则忽略." ,httpMethod="POST")
	public void export(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		List<TestOneToOneClass> list= testOneToOneClassService.selectAll(testOneToOneClass);
		Map<String, String> header = new LinkedHashMap<>();
		header.put("id", "班级id");
		header.put("userId", "用户id");
		header.put("className", "班级名称");
		header.put("quantity", "班级���数");
		header.put("createTime_", "create_time");
		ExcelUtils.exportExcel("一对一班级",header,list,response,request);

	}


	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneClass/selectTestOneToOneUserAndTestOneToOneClass")
	@ResponseBody
	@ApiOperation(value="外表级联查询(带分页)", notes="构建外表 级联查询(带分页) 默认 page=1 pageSize等于10 详见 Page类(所有bean都继承该类)" ,httpMethod="POST")
	public DataRes selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.selectTestOneToOneUserAndTestOneToOneClass(testOneToOneClass));
	}


	/**
	 * 级联条件查询 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneClass/selectTestOneToOneUserAndTestOneToOneClassByCondition")
	@ResponseBody
	@ApiOperation(value="外表级联条件查询", notes="外表级联条件查询" ,httpMethod="POST")
	public DataRes selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.selectTestOneToOneUserAndTestOneToOneClassByCondition(testOneToOneClass));
	}


	/**
	 * 级联删除(根据主键删除) 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneClass/deleteTestOneToOneUserAndTestOneToOneClass")
	@ResponseBody
	@ApiOperation(value="外表级联删除(根据主键删除)", notes="外表级联删除(根据主键删除)" ,httpMethod="POST")
	public DataRes deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneClass testOneToOneClass,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneClassService.deleteTestOneToOneUserAndTestOneToOneClass(testOneToOneClass));
	}



}
