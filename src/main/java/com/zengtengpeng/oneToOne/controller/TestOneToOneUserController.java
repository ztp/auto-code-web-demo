package com.zengtengpeng.oneToOne.controller;

import com.zengtengpeng.oneToOne.service.TestOneToOneClassService;
import com.zengtengpeng.oneToOne.bean.TestOneToOneClass;
import java.util.ArrayList;
import javax.annotation.Resource;
import com.zengtengpeng.common.utils.ExcelUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zengtengpeng.common.bean.DataRes;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.zengtengpeng.oneToOne.bean.TestOneToOneUser;
import com.zengtengpeng.oneToOne.service.TestOneToOneUserService;


/**
 *一对一用户 controller
 */
@Api(description="一对一用户")
@Controller
public class TestOneToOneUserController  {

	/**
	 * 一对一班级
	 */
	@Resource
	private TestOneToOneClassService testOneToOneClassService;



	@Resource
	private TestOneToOneUserService testOneToOneUserService;
	/**
	 * 删除-一对一用户
	 */
	@ResponseBody
	@RequestMapping("testOneToOneUser/deleteByPrimaryKey")
	@ApiOperation(value="根据主键删除", notes="参数只用到了主键id,其他参数忽略" ,httpMethod="POST")
	public DataRes deleteByPrimaryKey(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.deleteByPrimaryKey(testOneToOneUser));
	}


	/**
	 *  保存 (主键为空则增加否则修改)-> 一对一用户
	 */
	@ResponseBody
	@RequestMapping("testOneToOneUser/save")
	@ApiOperation(value="保存", notes="主键为空则增加否则修改" ,httpMethod="POST")
	public DataRes save(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		if(testOneToOneUser.getId()==null){
			return DataRes.success(testOneToOneUserService.insert(testOneToOneUser));
		}
		return DataRes.success(testOneToOneUserService.update(testOneToOneUser));

	}


	/**
	 * 根据主键查询->一对一用户
	 */
	@ResponseBody
	@RequestMapping("testOneToOneUser/selectByPrimaryKey")
	@ApiOperation(value="根据主键查询", notes="参数只用到了主键." ,httpMethod="POST")
	public DataRes selectByPrimaryKey(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.selectByPrimaryKey(testOneToOneUser));
	}


	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->一对一用户
	 */
	@ResponseBody
	@RequestMapping("testOneToOneUser/selectByCondition")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByCondition(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.selectByCondition(testOneToOneUser));
	}


	/**
	 * 分页查询 (所有的实体属性都是条件,如果为空则忽略该字段) (详见Page类.所以的实体都继承该类 默认 page=1 pageSize=10)->一对一用户
	 */
	@ResponseBody
	@RequestMapping("testOneToOneUser/selectAllByPaging")
	@ApiOperation(value="分页查询", notes="默认page=1pageSize等于10详见Page类(所有bean都继承该类).参数为空则忽略" ,httpMethod="POST")
	public DataRes selectAllByPaging(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.selectAllByPaging(testOneToOneUser));
	}


	/**
	 * 导出报表->一对一用户
	 */
	@RequestMapping("testOneToOneUser/export")
	@ApiOperation(value="导出excel", notes="导出全部数据.参数为空则忽略." ,httpMethod="POST")
	public void export(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		List<TestOneToOneUser> list= testOneToOneUserService.selectAll(testOneToOneUser);
		Map<String, String> header = new LinkedHashMap<>();
		header.put("id", "id");
		header.put("name", "名称");
		header.put("age", "年龄");
		header.put("status_", "{\"name\":\"状态\",\"1\":\"启用\",\"0\":\"禁用\"}");
		header.put("birthday_", "生日");
		header.put("remarks", "备注");
		header.put("mun", "数字");
		header.put("createTime_", "创建时间");
		header.put("updateTime_", "更新时间");
		ExcelUtils.exportExcel("一对一用户",header,list,response,request);

	}


	/**
	 * 级联查询(带分页) 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneUser/selectTestOneToOneUserAndTestOneToOneClass")
	@ResponseBody
	@ApiOperation(value="主表级联查询(带分页)", notes="主表级联查询(带分页)  默认 page=1 pageSize等于10 详见 Page类(所有bean都继承该类)" ,httpMethod="POST")
	public DataRes selectTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.selectTestOneToOneUserAndTestOneToOneClass(testOneToOneUser));
	}


	/**
	 * 级联条件查询 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneUser/selectTestOneToOneUserAndTestOneToOneClassByCondition")
	@ResponseBody
	@ApiOperation(value="主表级联条件查询", notes="主表级联条件查询" ,httpMethod="POST")
	public DataRes selectTestOneToOneUserAndTestOneToOneClassByCondition(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.selectTestOneToOneUserAndTestOneToOneClassByCondition(testOneToOneUser));
	}


	/**
	 * 级联删除(根据主键删除) 一对一用户--一对一班级
	 */
	@RequestMapping("testOneToOneUser/deleteTestOneToOneUserAndTestOneToOneClass")
	@ResponseBody
	@ApiOperation(value="主表级联删除(根据主键删除)", notes="主表级联删除(根据主键删除)" ,httpMethod="POST")
	public DataRes deleteTestOneToOneUserAndTestOneToOneClass(TestOneToOneUser testOneToOneUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testOneToOneUserService.deleteTestOneToOneUserAndTestOneToOneClass(testOneToOneUser));
	}



}
