package com.zengtengpeng.manyToMany.controller;

import com.zengtengpeng.manyToMany.service.TestManyToManyRoleService;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;
import java.util.ArrayList;
import javax.annotation.Resource;
import com.zengtengpeng.common.utils.ExcelUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zengtengpeng.common.bean.DataRes;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;
import com.zengtengpeng.manyToMany.service.TestManyToManyUserService;


/**
 *多对多用户 controller
 */
@Api(description="多对多用户")
@Controller
public class TestManyToManyUserController  {

	/**
	 * 多对多角色
	 */
	@Resource
	private TestManyToManyRoleService testManyToManyRoleService;



	@Resource
	private TestManyToManyUserService testManyToManyUserService;
	/**
	 * 删除-多对多用户
	 */
	@ResponseBody
	@RequestMapping("testManyToManyUser/deleteByPrimaryKey")
	@ApiOperation(value="根据主键删除", notes="参数只用到了主键id,其他参数忽略" ,httpMethod="POST")
	public DataRes deleteByPrimaryKey(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.deleteByPrimaryKey(testManyToManyUser));
	}


	/**
	 *  保存 (主键为空则增加否则修改)-> 多对多用户
	 */
	@ResponseBody
	@RequestMapping("testManyToManyUser/save")
	@ApiOperation(value="保存", notes="主键为空则增加否则修改" ,httpMethod="POST")
	public DataRes save(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		if(testManyToManyUser.getId()==null){
			return DataRes.success(testManyToManyUserService.insert(testManyToManyUser));
		}
		return DataRes.success(testManyToManyUserService.update(testManyToManyUser));

	}


	/**
	 * 根据主键查询->多对多用户
	 */
	@ResponseBody
	@RequestMapping("testManyToManyUser/selectByPrimaryKey")
	@ApiOperation(value="根据主键查询", notes="参数只用到了主键." ,httpMethod="POST")
	public DataRes selectByPrimaryKey(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.selectByPrimaryKey(testManyToManyUser));
	}


	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->多对多用户
	 */
	@ResponseBody
	@RequestMapping("testManyToManyUser/selectByCondition")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByCondition(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.selectByCondition(testManyToManyUser));
	}


	/**
	 * 分页查询 (所有的实体属性都是条件,如果为空则忽略该字段) (详见Page类.所以的实体都继承该类 默认 page=1 pageSize=10)->多对多用户
	 */
	@ResponseBody
	@RequestMapping("testManyToManyUser/selectAllByPaging")
	@ApiOperation(value="分页查询", notes="默认page=1pageSize等于10详见Page类(所有bean都继承该类).参数为空则忽略" ,httpMethod="POST")
	public DataRes selectAllByPaging(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.selectAllByPaging(testManyToManyUser));
	}


	/**
	 * 导出报表->多对多用户
	 */
	@RequestMapping("testManyToManyUser/export")
	@ApiOperation(value="导出excel", notes="导出全部数据.参数为空则忽略." ,httpMethod="POST")
	public void export(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		List<TestManyToManyUser> list= testManyToManyUserService.selectAll(testManyToManyUser);
		Map<String, String> header = new LinkedHashMap<>();
		header.put("id", "id");
		header.put("name", "名称");
		header.put("age", "年龄");
		header.put("status_", "{\"name\":\"状态\",\"1\":\"启用\",\"0\":\"禁用\"}");
		header.put("birthday_", "生日");
		header.put("remarks", "备注");
		header.put("mun", "数字");
		header.put("createTime_", "创建时间");
		header.put("updateTime_", "更新时间");
		ExcelUtils.exportExcel("多对多用户",header,list,response,request);

	}


	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyUser/selectTestManyToManyUserAndTestManyToManyRole")
	@ResponseBody
	@ApiOperation(value="主表级联查询(带分页)", notes="主表级联查询(带分页)  默认 page=1 pageSize等于10 详见 Page类(所有bean都继承该类)" ,httpMethod="POST")
	public DataRes selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.selectTestManyToManyUserAndTestManyToManyRole(testManyToManyUser));
	}


	/**
	 * 级联条件查询 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyUser/selectTestManyToManyUserAndTestManyToManyRoleByCondition")
	@ResponseBody
	@ApiOperation(value="主表级联条件查询", notes="主表级联条件查询" ,httpMethod="POST")
	public DataRes selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.selectTestManyToManyUserAndTestManyToManyRoleByCondition(testManyToManyUser));
	}


	/**
	 * 级联删除(根据主键删除) 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyUser/deleteTestManyToManyUserAndTestManyToManyRole")
	@ResponseBody
	@ApiOperation(value="主表级联删除(根据主键删除)", notes="主表级联删除(根据主键删除)" ,httpMethod="POST")
	public DataRes deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyUserService.deleteTestManyToManyUserAndTestManyToManyRole(testManyToManyUser));
	}


	/**
	 * 根据外表id查询主表表所有数据(带分页)
	 */
	@RequestMapping("testManyToManyUser/selectTestManyToManyUserByTestManyToManyRole")
	@ResponseBody
	@ApiOperation(value="根据外表id查询主表表所有数据(带分页)", notes="根据外表id查询主表表所有数据(带分页)" ,httpMethod="POST")
	public DataRes selectTestManyToManyUserByTestManyToManyRole(HttpServletRequest request,HttpServletResponse response,TestManyToManyUser testManyToManyUser){
		return DataRes.success(testManyToManyUserService.selectTestManyToManyUserByTestManyToManyRole(testManyToManyUser));
	}



}
