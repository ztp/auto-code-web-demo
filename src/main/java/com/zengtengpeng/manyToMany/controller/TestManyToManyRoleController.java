package com.zengtengpeng.manyToMany.controller;

import com.zengtengpeng.manyToMany.service.TestManyToManyUserService;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;
import java.util.ArrayList;
import javax.annotation.Resource;
import com.zengtengpeng.common.utils.ExcelUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zengtengpeng.common.bean.DataRes;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;
import com.zengtengpeng.manyToMany.service.TestManyToManyRoleService;


/**
 *多对多角色 controller
 */
@Api(description="多对多角色")
@Controller
public class TestManyToManyRoleController  {

	/**
	 * 多对多用户
	 */
	@Resource
	private TestManyToManyUserService testManyToManyUserService;



	@Resource
	private TestManyToManyRoleService testManyToManyRoleService;
	/**
	 * 删除-多对多角色
	 */
	@ResponseBody
	@RequestMapping("testManyToManyRole/deleteByPrimaryKey")
	@ApiOperation(value="根据主键删除", notes="参数只用到了主键id,其他参数忽略" ,httpMethod="POST")
	public DataRes deleteByPrimaryKey(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.deleteByPrimaryKey(testManyToManyRole));
	}


	/**
	 *  保存 (主键为空则增加否则修改)-> 多对多角色
	 */
	@ResponseBody
	@RequestMapping("testManyToManyRole/save")
	@ApiOperation(value="保存", notes="主键为空则增加否则修改" ,httpMethod="POST")
	public DataRes save(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		if(testManyToManyRole.getId()==null){
			return DataRes.success(testManyToManyRoleService.insert(testManyToManyRole));
		}
		return DataRes.success(testManyToManyRoleService.update(testManyToManyRole));

	}


	/**
	 * 根据主键查询->多对多角色
	 */
	@ResponseBody
	@RequestMapping("testManyToManyRole/selectByPrimaryKey")
	@ApiOperation(value="根据主键查询", notes="参数只用到了主键." ,httpMethod="POST")
	public DataRes selectByPrimaryKey(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.selectByPrimaryKey(testManyToManyRole));
	}


	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->多对多角色
	 */
	@ResponseBody
	@RequestMapping("testManyToManyRole/selectByCondition")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByCondition(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.selectByCondition(testManyToManyRole));
	}


	/**
	 * 分页查询 (所有的实体属性都是条件,如果为空则忽略该字段) (详见Page类.所以的实体都继承该类 默认 page=1 pageSize=10)->多对多角色
	 */
	@ResponseBody
	@RequestMapping("testManyToManyRole/selectAllByPaging")
	@ApiOperation(value="分页查询", notes="默认page=1pageSize等于10详见Page类(所有bean都继承该类).参数为空则忽略" ,httpMethod="POST")
	public DataRes selectAllByPaging(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.selectAllByPaging(testManyToManyRole));
	}


	/**
	 * 导出报表->多对多角色
	 */
	@RequestMapping("testManyToManyRole/export")
	@ApiOperation(value="导出excel", notes="导出全部数据.参数为空则忽略." ,httpMethod="POST")
	public void export(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		List<TestManyToManyRole> list= testManyToManyRoleService.selectAll(testManyToManyRole);
		Map<String, String> header = new LinkedHashMap<>();
		header.put("id", "角色");
		header.put("name", "角色名称");
		header.put("status_", "{\"name\":\"状态\",\"0\":\"启用\",\"1\":\"禁用\"}");
		header.put("createUserId", "创建者");
		header.put("createTime_", "创建时间");
		header.put("updateUserId", "更新者");
		header.put("updateTime_", "更新时间");
		header.put("dels_", "{\"name\":\"是否删除\",\"0\":\"正常\",\"1\":\"删除\"}");
		ExcelUtils.exportExcel("多对多角色",header,list,response,request);

	}


	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyRole/selectTestManyToManyUserAndTestManyToManyRole")
	@ResponseBody
	@ApiOperation(value="外表级联查询(带分页)", notes="构建外表 级联查询(带分页) 默认 page=1 pageSize等于10 详见 Page类(所有bean都继承该类)" ,httpMethod="POST")
	public DataRes selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.selectTestManyToManyUserAndTestManyToManyRole(testManyToManyRole));
	}


	/**
	 * 级联条件查询 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyRole/selectTestManyToManyUserAndTestManyToManyRoleByCondition")
	@ResponseBody
	@ApiOperation(value="外表级联条件查询", notes="外表级联条件查询" ,httpMethod="POST")
	public DataRes selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.selectTestManyToManyUserAndTestManyToManyRoleByCondition(testManyToManyRole));
	}


	/**
	 * 级联删除(根据主键删除) 多对多用户--多对多角色
	 */
	@RequestMapping("testManyToManyRole/deleteTestManyToManyUserAndTestManyToManyRole")
	@ResponseBody
	@ApiOperation(value="外表级联删除(根据主键删除)", notes="外表级联删除(根据主键删除)" ,httpMethod="POST")
	public DataRes deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testManyToManyRoleService.deleteTestManyToManyUserAndTestManyToManyRole(testManyToManyRole));
	}


	/**
	 * 根据主表id查询外表所有数据(带分页)
	 */
	@RequestMapping("testManyToManyRole/selectTestManyToManyRoleByTestManyToManyUser")
	@ResponseBody
	@ApiOperation(value="根据主表id查询外表所有数据(带分页)", notes="根据主表id查询外表所有数据(带分页)" ,httpMethod="POST")
	public DataRes selectTestManyToManyRoleByTestManyToManyUser(HttpServletRequest request,HttpServletResponse response,TestManyToManyRole testManyToManyRole){
		return DataRes.success(testManyToManyRoleService.selectTestManyToManyRoleByTestManyToManyUser(testManyToManyRole));
	}



}
