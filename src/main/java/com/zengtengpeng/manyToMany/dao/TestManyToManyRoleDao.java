package com.zengtengpeng.manyToMany.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.zengtengpeng.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;


/**
 *多对多角色 dao
 */
@Mapper
public interface TestManyToManyRoleDao  extends BaseDao<TestManyToManyRole>{




	/**
	 * 根据多对多用户查询多对多角色
	 */
	public List<TestManyToManyRole> selectTestManyToManyRoleByTestManyToManyUser(TestManyToManyRole testManyToManyRole);
	/**
	 * 根据多对多用户删除多对多角色
	 */
	public Integer deleteTestManyToManyRoleByTestManyToManyUser(TestManyToManyRole testManyToManyRole);
	/**
	 * 级联新增
	 */
	public Integer insertRelation(@Param("id") String id,@Param("userId") String[] userId);

}
