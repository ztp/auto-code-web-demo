package com.zengtengpeng.manyToMany.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.zengtengpeng.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;


/**
 *多对多用户 dao
 */
@Mapper
public interface TestManyToManyUserDao  extends BaseDao<TestManyToManyUser>{




	/**
	 * 根据多对多角色查询多对多用户
	 */
	public List<TestManyToManyUser> selectTestManyToManyUserByTestManyToManyRole(TestManyToManyUser testManyToManyUser);
	/**
	 * 根据多对多角色删除多对多用户
	 */
	public Integer deleteTestManyToManyUserByTestManyToManyRole(TestManyToManyUser testManyToManyUser);
	/**
	 * 级联新增
	 */
	public Integer insertRelation(@Param("id") String id,@Param("roleId") String[] roleId);

}
