package com.zengtengpeng.manyToMany.service.impl;

import com.zengtengpeng.manyToMany.dao.TestManyToManyRoleDao;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zengtengpeng.manyToMany.dao.TestManyToManyUserDao;
import com.zengtengpeng.manyToMany.service.TestManyToManyUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;


/**
 *多对多用户 serverImpl
 */
@Service
@Transactional
public class TestManyToManyUserServiceImpl   implements TestManyToManyUserService {

	/**
	 * 多对多角色
	 */
	@Resource
	private TestManyToManyRoleDao testManyToManyRoleDao;



	/**
	 * 注入dao
	 */
	@Resource
	private TestManyToManyUserDao testManyToManyUserDao;
	/**
	 * 初始化
	 */
	@Override
	public TestManyToManyUserDao initDao(){
		return testManyToManyUserDao;
	}


	/**
	 * 多对多重写insert
	 */
	@Override
	public int insert(TestManyToManyUser testManyToManyUser){
		int insert = testManyToManyUserDao.insert(testManyToManyUser);
		String id = testManyToManyUser.getRoleId();
		if(id !=null && !"".equals(id)){
			String[] split = id.split(",");
			testManyToManyUserDao.insertRelation(testManyToManyUser.getId().toString(),split);
		}
		return insert;

	}


	/**
	 * 多对多重写Update
	 */
	@Override
	public int update(TestManyToManyUser testManyToManyUser){
		Integer update = testManyToManyUserDao.update(testManyToManyUser);
		String id = testManyToManyUser.getRoleId();
		if(id !=null && !"".equals(id)){
			TestManyToManyRole d=new TestManyToManyRole();
			d.setUserId(testManyToManyUser.getId().toString());
			testManyToManyRoleDao.deleteTestManyToManyRoleByTestManyToManyUser(d);
			String[] split = id.split(",");
			testManyToManyUserDao.insertRelation(testManyToManyUser.getId().toString(),split);
		}
		return update;

	}


	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	@Override
	public TestManyToManyUser selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser){
		testManyToManyUser = this.selectAllByPaging(testManyToManyUser);
		if(testManyToManyUser!=null && testManyToManyUser.getRows()!=null){
			testManyToManyUser.getRows().forEach(t->{
				TestManyToManyUser data= (TestManyToManyUser) t;
				TestManyToManyRole testManyToManyRole=new TestManyToManyRole();
				testManyToManyRole.setUserId(data.getId().toString());
				List<TestManyToManyRole> datas=testManyToManyRoleDao.selectTestManyToManyRoleByTestManyToManyUser(testManyToManyRole);
				data.setTestManyToManyRoleList(datas);
			});
		}
		return testManyToManyUser;

	}


	/**
	 * 级联条件查询 多对多用户--多对多角色
	 */
	@Override
	public List<TestManyToManyUser> selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyUser testManyToManyUser){
		List<TestManyToManyUser> datas = this.selectByCondition(testManyToManyUser);
		if(datas!=null){
			datas.forEach(t->{
				TestManyToManyRole testManyToManyRole=new TestManyToManyRole();
				testManyToManyRole.setUserId(t.getId().toString());
				List<TestManyToManyRole> lists=testManyToManyRoleDao.selectTestManyToManyRoleByTestManyToManyUser(testManyToManyRole);
				t.setTestManyToManyRoleList(lists);
			});
		}
		return datas;

	}


	/**
	 * 级联删除(根据主表删除) 多对多用户--多对多角色
	 */
	@Override
	public Integer deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser){
		TestManyToManyRole testManyToManyRole=new TestManyToManyRole();
		testManyToManyUser = testManyToManyUserDao.selectByPrimaryKey(testManyToManyUser);
		testManyToManyRole.setUserId(testManyToManyUser.getId().toString());
		testManyToManyRoleDao.deleteTestManyToManyRoleByTestManyToManyUser(testManyToManyRole);
		return testManyToManyUserDao.deleteByPrimaryKey(testManyToManyUser);

	}


	/**
	 * 根据外表id查询主表所有数据(带分页)
	 */
	@Override
	public TestManyToManyUser selectTestManyToManyUserByTestManyToManyRole(TestManyToManyUser t){
		PageHelper.startPage(t.getPage(), t.getPageSize());
		List<TestManyToManyUser> lists = testManyToManyUserDao.selectTestManyToManyUserByTestManyToManyRole(t);
		PageInfo pageInfo = new PageInfo(lists);
		t.setRows(lists);
		t.setTotal((new Long(pageInfo.getTotal())).intValue());
		return t;

	}



}
