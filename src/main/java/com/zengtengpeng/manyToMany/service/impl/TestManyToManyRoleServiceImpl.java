package com.zengtengpeng.manyToMany.service.impl;

import com.zengtengpeng.manyToMany.dao.TestManyToManyUserDao;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zengtengpeng.manyToMany.dao.TestManyToManyRoleDao;
import com.zengtengpeng.manyToMany.service.TestManyToManyRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;


/**
 *多对多角色 serverImpl
 */
@Service
@Transactional
public class TestManyToManyRoleServiceImpl   implements TestManyToManyRoleService {

	/**
	 * 多对多角色
	 */
	@Resource
	private TestManyToManyUserDao testManyToManyUserDao;



	/**
	 * 注入dao
	 */
	@Resource
	private TestManyToManyRoleDao testManyToManyRoleDao;
	/**
	 * 初始化
	 */
	@Override
	public TestManyToManyRoleDao initDao(){
		return testManyToManyRoleDao;
	}


	/**
	 * 多对多重写insert
	 */
	@Override
	public int insert(TestManyToManyRole testManyToManyRole){
		int insert = testManyToManyRoleDao.insert(testManyToManyRole);
		String id = testManyToManyRole.getUserId();
		if(id !=null && !"".equals(id)){
			String[] split = id.split(",");
			testManyToManyRoleDao.insertRelation(testManyToManyRole.getId().toString(),split);
		}
		return insert;

	}


	/**
	 * 多对多重写Update
	 */
	@Override
	public int update(TestManyToManyRole testManyToManyRole){
				Integer update = testManyToManyRoleDao.update(testManyToManyRole);
		String id = testManyToManyRole.getUserId();
		if(id !=null && !"".equals(id)){
			TestManyToManyUser d=new TestManyToManyUser();
			d.setRoleId(testManyToManyRole.getId().toString());
			testManyToManyUserDao.deleteTestManyToManyUserByTestManyToManyRole(d);
			String[] split = id.split(",");
			testManyToManyRoleDao.insertRelation(testManyToManyRole.getId().toString(),split);
		}
		return update;

	}


	/**
	 * 级联查询(带分页) 多对多角色--多对多用户
	 */
	@Override
	public TestManyToManyRole selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole){
		testManyToManyRole = this.selectAllByPaging(testManyToManyRole);
		if(testManyToManyRole!=null && testManyToManyRole.getRows()!=null){
			testManyToManyRole.getRows().forEach(t->{
				TestManyToManyRole data= (TestManyToManyRole) t;
				TestManyToManyUser testManyToManyUser=new TestManyToManyUser();
				testManyToManyUser.setRoleId(data.getId().toString());
				List<TestManyToManyUser> datas=testManyToManyUserDao.selectTestManyToManyUserByTestManyToManyRole(testManyToManyUser);
				data.setTestManyToManyUserList(datas);
			});
		}
		return testManyToManyRole;

	}


	/**
	 * 级联条件查询多对多用户--多对多角色
	 */
	@Override
	public List<TestManyToManyRole> selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyRole testManyToManyRole){
		List<TestManyToManyRole> datas = this.selectByCondition(testManyToManyRole);
		if(datas!=null){
			datas.forEach(t->{
				TestManyToManyUser testManyToManyUser=new TestManyToManyUser();
				testManyToManyUser.setRoleId(t.getId().toString());
				List<TestManyToManyUser> lists=testManyToManyUserDao.selectTestManyToManyUserByTestManyToManyRole(testManyToManyUser);
				t.setTestManyToManyUserList(lists);
			});
		}
		return datas;

	}


	/**
	 * 级联删除(根据主表删除) 多对多角色--多对多用户
	 */
	@Override
	public Integer deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole){
		TestManyToManyUser testManyToManyUser=new TestManyToManyUser();
		testManyToManyRole = testManyToManyRoleDao.selectByPrimaryKey(testManyToManyRole);
		testManyToManyUser.setRoleId(testManyToManyRole.getId().toString());
		testManyToManyUserDao.deleteTestManyToManyUserByTestManyToManyRole(testManyToManyUser);
		return testManyToManyRoleDao.deleteByPrimaryKey(testManyToManyRole);

	}


	/**
	 * 根据主表id查询外表所有数据(带分页)
	 */
	@Override
	public TestManyToManyRole selectTestManyToManyRoleByTestManyToManyUser(TestManyToManyRole t){
		PageHelper.startPage(t.getPage(), t.getPageSize());
		List<TestManyToManyRole> lists = testManyToManyRoleDao.selectTestManyToManyRoleByTestManyToManyUser(t);
		PageInfo pageInfo = new PageInfo(lists);
		t.setRows(lists);
		t.setTotal((new Long(pageInfo.getTotal())).intValue());
		return t;

	}



}
