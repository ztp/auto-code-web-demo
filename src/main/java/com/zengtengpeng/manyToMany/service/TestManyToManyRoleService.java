package com.zengtengpeng.manyToMany.service;

import java.util.List;
import com.zengtengpeng.common.service.BaseService;
import com.zengtengpeng.manyToMany.bean.TestManyToManyRole;
import com.zengtengpeng.manyToMany.dao.TestManyToManyRoleDao;


/**
 *多对多角色 service
 */
public interface TestManyToManyRoleService extends BaseService<TestManyToManyRole,TestManyToManyRoleDao>{




	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	public TestManyToManyRole selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole);
	/**
	 * 级联条件查询 多对多用户--多对多角色
	 */
	public List<TestManyToManyRole> selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyRole testManyToManyRole);
	/**
	 * 级联删除(根据主键删除) 多对多用户--多对多角色
	 */
	public Integer deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyRole testManyToManyRole);
	/**
	 * 根据主表id查询外表所有数据(带分页)
	 */
	public TestManyToManyRole selectTestManyToManyRoleByTestManyToManyUser(TestManyToManyRole testManyToManyRole);

}
