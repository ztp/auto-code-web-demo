package com.zengtengpeng.manyToMany.service;

import java.util.List;
import com.zengtengpeng.common.service.BaseService;
import com.zengtengpeng.manyToMany.bean.TestManyToManyUser;
import com.zengtengpeng.manyToMany.dao.TestManyToManyUserDao;


/**
 *多对多用户 service
 */
public interface TestManyToManyUserService extends BaseService<TestManyToManyUser,TestManyToManyUserDao>{




	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	public TestManyToManyUser selectTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser);
	/**
	 * 级联查询(带分页) 多对多用户--多对多角色
	 */
	public List<TestManyToManyUser> selectTestManyToManyUserAndTestManyToManyRoleByCondition(TestManyToManyUser testManyToManyUser);
	/**
	 * 级联删除(根据主键删除) 多对多用户--多对多角色
	 */
	public Integer deleteTestManyToManyUserAndTestManyToManyRole(TestManyToManyUser testManyToManyUser);
	/**
	 * 根据外表id查询主表表所有数据(带分页)
	 */
	public TestManyToManyUser selectTestManyToManyUserByTestManyToManyRole(TestManyToManyUser testManyToManyUser);

}
