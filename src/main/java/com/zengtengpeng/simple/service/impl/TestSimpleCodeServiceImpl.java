package com.zengtengpeng.simple.service.impl;
import com.zengtengpeng.simple.dao.TestSimpleCodeDao;
import com.zengtengpeng.simple.service.TestSimpleCodeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;


/**
 *单表代码生成 serverImpl
 */
@Service
@Transactional
public class TestSimpleCodeServiceImpl   implements TestSimpleCodeService {


	/**
	 * 注入dao
	 */
	@Resource
	private TestSimpleCodeDao testSimpleCodeDao;
	/**
	 * 初始化
	 */
	@Override
	public TestSimpleCodeDao initDao(){
		return testSimpleCodeDao;
	}


}
