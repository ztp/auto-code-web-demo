package com.zengtengpeng.simple.service;
import com.zengtengpeng.common.service.BaseService;
import com.zengtengpeng.simple.bean.TestSimpleCode;
import com.zengtengpeng.simple.dao.TestSimpleCodeDao;


/**
 *单表代码生成 service
 */
public interface TestSimpleCodeService extends BaseService<TestSimpleCode,TestSimpleCodeDao>{


}
