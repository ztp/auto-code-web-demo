package com.zengtengpeng.simple.controller;
import javax.annotation.Resource;
import com.zengtengpeng.common.utils.ExcelUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zengtengpeng.common.bean.DataRes;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.zengtengpeng.simple.bean.TestSimpleCode;
import com.zengtengpeng.simple.service.TestSimpleCodeService;


/**
 *单表代码生成 controller
 */
@Api(description="单表代码生成")
@Controller
public class TestSimpleCodeController  {


	@Resource
	private TestSimpleCodeService testSimpleCodeService;
	/**
	 * 删除-单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/deleteByPrimaryKey")
	@ApiOperation(value="根据主键删除", notes="参数只用到了主键id,其他参数忽略" ,httpMethod="POST")
	public DataRes deleteByPrimaryKey(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testSimpleCodeService.deleteByPrimaryKey(testSimpleCode));
	}


	/**
	 *  保存 (主键为空则增加否则修改)-> 单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/save")
	@ApiOperation(value="保存", notes="主键为空则增加否则修改" ,httpMethod="POST")
	public DataRes save(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		if(testSimpleCode.getId()==null){
			return DataRes.success(testSimpleCodeService.insert(testSimpleCode));
		}
		return DataRes.success(testSimpleCodeService.update(testSimpleCode));

	}


	/**
	 * 根据主键查询->单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/selectByPrimaryKey")
	@ApiOperation(value="根据主键查询", notes="参数只用到了主键." ,httpMethod="POST")
	public DataRes selectByPrimaryKey(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testSimpleCodeService.selectByPrimaryKey(testSimpleCode));
	}


	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/selectByCondition")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByCondition(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testSimpleCodeService.selectByCondition(testSimpleCode));
	}
	/**
	 * 根据条件查询(所有的实体属性都是条件,如果为空则忽略该字段)->单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/selectByConditionFirst")
	@ApiOperation(value="根据条件查询", notes="参数为空则忽略." ,httpMethod="POST")
	public DataRes selectByConditionFirst(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testSimpleCodeService.selectByConditionFirst(testSimpleCode));
	}


	/**
	 * 分页查询 (所有的实体属性都是条件,如果为空则忽略该字段) (详见Page类.所以的实体都继承该类 默认 page=1 pageSize=10)->单表代码生成
	 */
	@ResponseBody
	@RequestMapping("testSimpleCode/selectAllByPaging")
	@ApiOperation(value="分页查询", notes="默认page=1pageSize等于10详见Page类(所有bean都继承该类).参数为空则忽略" ,httpMethod="POST")
	public DataRes selectAllByPaging(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		return DataRes.success(testSimpleCodeService.selectAllByPaging(testSimpleCode));
	}


	/**
	 * 导出报表->单表代码生成
	 */
	@RequestMapping("testSimpleCode/export")
	@ApiOperation(value="导出excel", notes="导出全部数据.参数为空则忽略." ,httpMethod="POST")
	public void export(TestSimpleCode testSimpleCode,HttpServletRequest request,HttpServletResponse response){
		List<TestSimpleCode> list= testSimpleCodeService.selectAll(testSimpleCode);
		Map<String, String> header = new LinkedHashMap<>();
		header.put("id", "主键");
		header.put("name", "名称");
		header.put("age", "年龄");
		header.put("status_", "{\"name\":\"状态\",\"1\":\"启用\",\"0\":\"禁用\"}");
		header.put("birthday_", "生日");
		header.put("remarks", "备注");
		header.put("createTime_", "创建时间");
		ExcelUtils.exportExcel("单表代码生成",header,list,response,request);

	}


}
