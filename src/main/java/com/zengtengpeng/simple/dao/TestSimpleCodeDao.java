package com.zengtengpeng.simple.dao;
import com.zengtengpeng.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.zengtengpeng.simple.bean.TestSimpleCode;


/**
 *单表代码生成 dao
 */
@Mapper
public interface TestSimpleCodeDao  extends BaseDao<TestSimpleCode>{


}
